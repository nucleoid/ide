const fs = require("fs");

module.exports = function (app) {
  app.get("/", (req, res) =>
    res.sendFile("nucleoid.html", {
      root: "/opt/nucleoid/ide/",
    })
  );

  app.get("/custom.js", (req, res) => {
    if (fs.existsSync("/opt/nucleoid/custom.js")) {
      res.sendFile("custom.js", {
        root: "/opt/nucleoid/",
      });
    } else {
      res.send("");
    }
  });

  app.get("*.*", (req, res) =>
    res.sendFile(req.url.slice(1), {
      root: "/opt/nucleoid/ide/",
    })
  );
};
