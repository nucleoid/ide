function displayMessage(string) {
  $("#result").html(string);
}

function displayJSON(json) {
  $("#result").empty();
  const tree = JsonView.createTree(json);
  JsonView.render(tree, document.querySelector("#result"));
  JsonView.expandChildren(tree);
}

function displayTime(time) {
  $("#tick").show();
  $("#time").html(`Time: ${time.substring(13)} ms`);
}

$(function () {
  $("#top").resizable({
    handles: "n, s",
    minHeight: 50,
    maxHeight: $(window).height() - 50,
  });
  $("#top").resize(function () {
    $("#bottom").height($("#panel").height() - $("#top").height() - 2 - 20);
  });

  $("#sidebar").resizable({
    handles: "e, w",
    minWidth: 300,
    maxWidth: 600,
  });
  $("#sidebar").resize(function () {
    $("#panel").css("left", $("#sidebar").width());
    $("#panel").css("width", $(window).width() - $("#sidebar").width());
  });
});
